package ru.tsc.tambovtsev.tm.logger.consumer.listener;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.tambovtsev.tm.logger.consumer.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public final class EntityListener implements MessageListener {

    @Autowired
    private final LoggerService loggerService;

    public EntityListener(LoggerService loggerService) {
        this.loggerService = loggerService;
    }

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        final boolean checkType = message instanceof TextMessage;
        if (!checkType) return;
        @NonNull final TextMessage textMessage = (TextMessage) message;
        @NotNull final String json = textMessage.getText();
        System.out.println(json);
        loggerService.log(json);
    }

}
