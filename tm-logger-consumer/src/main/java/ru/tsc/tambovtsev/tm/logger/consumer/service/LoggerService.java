package ru.tsc.tambovtsev.tm.logger.consumer.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
public class LoggerService {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final MongoClient mongoClient = new MongoClient("localhost", 27017);

    private final MongoDatabase mongoDatabase = mongoClient.getDatabase("example");

    @SneakyThrows
    public void log(final String json) {
        final Map<String, Object> event = objectMapper.readValue(json, LinkedHashMap.class);
        final String collectionName = event.get("table").toString();
        if (mongoDatabase.getCollection(collectionName) == null) mongoDatabase.createCollection(collectionName);
        final MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
        collection.insertOne(new Document(event));
    }

}
