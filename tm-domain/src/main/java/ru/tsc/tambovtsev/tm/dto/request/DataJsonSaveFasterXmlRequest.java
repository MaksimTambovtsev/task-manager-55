package ru.tsc.tambovtsev.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataJsonSaveFasterXmlRequest extends AbstractUserRequest {

    public DataJsonSaveFasterXmlRequest(@Nullable String token) {
        super(token);
    }

}
