package ru.tsc.tambovtsev.tm.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.dto.request.DataJsonSaveJaxBRequest;
import ru.tsc.tambovtsev.tm.enumerated.Role;

@Component
public class DomainSaveJsonJaxbCommand extends AbstractDomainCommand {

    @NotNull
    private final static String NAME = "save-json-jaxb";

    @NotNull
    private final static String DESCRIPTION = "Save projects, tasks and users in json file";

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().saveDataJsonJaxb(new DataJsonSaveJaxBRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
