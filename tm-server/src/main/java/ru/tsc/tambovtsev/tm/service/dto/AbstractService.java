package ru.tsc.tambovtsev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.dto.IRepository;
import ru.tsc.tambovtsev.tm.api.service.IConnectionService;
import ru.tsc.tambovtsev.tm.api.service.dto.IService;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.dto.model.AbstractEntityDTO;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Optional;

public abstract class AbstractService<M extends AbstractEntityDTO, R extends IRepository<M>> implements IService<M> {

    @Nullable
    protected final R repository;

    @NotNull
    protected final IConnectionService connection;

    @NotNull
    public abstract IRepository<M> getRepository(@NotNull EntityManager entityManager);

    @Override
    public abstract void addAll(@NotNull final Collection<M> models);

    public AbstractService(@Nullable R repository, @NotNull IConnectionService connection) {
        this.repository = repository;
        this.connection = connection;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IRepository<M> repository = getRepository(entityManager);
        try {
            @Nullable final M result = repository.findById(id);
            return result;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IRepository<M> repository = getRepository(entityManager);
        try {
            @Nullable final M result = repository.findById(id);
            if (result == null) return;
            entityManager.getTransaction().begin();
            repository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(@Nullable final M model) {
        Optional.ofNullable(model).orElseThrow(NullPointerException::new);
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IRepository<M> repository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.create(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void update(@NotNull final M model) {
        @NotNull final EntityManager entityManager = connection.getEntityManager();
        @NotNull final IRepository<M> repository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
