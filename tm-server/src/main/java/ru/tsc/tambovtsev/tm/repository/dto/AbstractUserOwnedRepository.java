package ru.tsc.tambovtsev.tm.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.dto.IOwnerRepository;
import ru.tsc.tambovtsev.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModelDTO> extends AbstractRepository<M>
        implements IOwnerRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        entityManager
                .createQuery("DELETE FROM " + getTableName() + " WHERE USER_ID = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Nullable
    @Override
    public abstract M findById(@Nullable final String userId, @Nullable final String id);

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM " + getTableName() + " e WHERE USER_ID = :userId", Long.class)
                .setParameter("userId", userId)
                .getResultList()
                .get(0);
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        entityManager
                .createQuery("DELETE FROM " + getTableName() + " WHERE USER_ID = :userId AND ID = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

}
