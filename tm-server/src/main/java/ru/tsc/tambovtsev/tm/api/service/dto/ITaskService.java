package ru.tsc.tambovtsev.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskService extends IUserOwnedService<TaskDTO> {

    @Nullable
    TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    List<TaskDTO> findAll();

    void clearTask();

}
