package ru.tsc.tambovtsev.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.AbstractUserOwnedModel;

public interface IOwnerRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(@Nullable String userId);

    @Nullable
    M findById(@Nullable String userId, @Nullable String id);

    long getSize(@Nullable String userId);

    void removeById(@Nullable String userId, @Nullable String id);

}
