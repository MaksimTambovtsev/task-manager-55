package ru.tsc.tambovtsev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository extends IRepository<UserDTO> {

    void create(@Nullable UserDTO user);

    void lockUserById(@NotNull String id);

    void unlockUserById(@NotNull String id);

    void updateUser(@NotNull UserDTO user);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    void removeByLogin(@Nullable String login);

    @Nullable
    UserDTO findById(@Nullable String id);

    @Nullable
    List<UserDTO> findAllUser();

    void clearUser();

}
