package ru.tsc.tambovtsev.tm.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.repository.model.IProjectRepository;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    private static final String TABLE_NAME = "Project";

    public ProjectRepository(@NotNull final EntityManager connection) {
        super(connection);
    }

    @Override
    public @Nullable Project findById(@Nullable String userId, @Nullable String id) {
        return getResult(entityManager
                .createQuery("FROM " + getTableName() + " WHERE ID = :id AND USER_ID = :userId", Project.class)
                .setParameter("id", id)
                .setParameter("userId", userId));
    }

    @Override
    public @Nullable List<Project> findAll(@Nullable String userId, @Nullable Sort sort) {
        return entityManager
                .createQuery("FROM " + TABLE_NAME + " ORDER BY " + sort.name() + " ASC", Project.class)
                .getResultList();
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public @Nullable Project findById(@Nullable String id) {
        return entityManager.find(Project.class, id);
    }

    @Override
    @SneakyThrows
    public void updateById(@NotNull Project project) {
        entityManager
                .merge(project);
    }

    @Override
    public @Nullable List<Project> findAllProject() {
        return entityManager
                .createQuery("FROM " + TABLE_NAME, Project.class)
                .getResultList();
    }

    @Override
    public void removeByIdProject(@Nullable String userId, @Nullable String id) {
        entityManager
                .createQuery("DELETE FROM " + TABLE_NAME + " WHERE USER_ID = :userId AND ID = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void clearProject() {
        entityManager
                .createQuery("DELETE FROM " + TABLE_NAME)
                .executeUpdate();
    }

    @Override
    public void addAll(@NotNull Collection<Project> models) {
        entityManager
                .persist(models);
    }

}
